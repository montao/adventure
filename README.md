# Adventure World [![Build Status](https://travis-ci.org/montao/adventure.svg?branch=master&p=v)](https://travis-ci.org/montao/adventure) [![Coverage Status](https://coveralls.io/repos/github/montao/adventure/badge.svg?branch=master)](https://coveralls.io/github/montao/adventure?branch=master)

![](https://raw.githubusercontent.com/montao/adventure/master/Screenshot_20171229-002209.png)

Please write the issues at the github repository www.github.com/montao/adventure/issues

The following classes are defined. 

##### AdventureActivity
The UI class

##### World
The data structure that describes the world

##### AdventureWorld
Subclass to World that builds up the adventure world. This class should be supplemented so that own items and objects also are in the world. 

##### Place
The data type for places. Each Place-object contains information about which persons that are there and how to enter other places.

##### Person
The data type for persons. NPC with specific properties subclass the person class. 

##### Thing
The data type for items. There are subclasses for Thing for different items that appear at different places. 

##### Player
A Player object makes it possible to control a special person: You. This object is displayed graphically with the UI and control panel where the user can both control and follow the game engine events and actions. 

##### PlaceView
Graphical object which is used by Player to display the world from the user's viewpoint. 

##### Commands
Used by player to give the user possibility of control. 

##### PriorityQueue
Abstract data type for a priority queue. 
