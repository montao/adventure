package dev.game.adventure;

import android.graphics.drawable.Drawable;
import android.widget.TextView;


public class Adventure {

    private Player player;

    public Player getPlayer() {
        return player;
    }

    public Adventure(TextView t, AdventureActivity target, PlaceView placeView) {
        World world = new AdventureWorld(this, t, target, placeView);
        Person me = new Person(world, "You", 0, target);
        placeView.mainCharacter = me;
        player = new Player(world, me, t);
    }


    public Drawable loadPicture(int imageName, AdventureActivity target) {
        Drawable im = target.getResources().getDrawable(imageName);
        return im;
    }
}