package dev.game.adventure;

import java.util.*;

/**
 * The data structure that describes the world.
 */
public abstract class World { // "implements" Playable
    public Adventure owner;

    public PlaceView ag;

    public PlaceView getAg() {
        return ag;
    }

    /**
     * Players watching this World
     */
    public Collection<Player> players;

    public void setAg(PlaceView ag) {
        this.ag = ag;
    }

    /**
     * Places in this World
     */
    public Map<String, Place> places;

    public Map<String, Place> getPlaces() {
        return places;
    }

    public void setPlaces(Map<String, Place> places) {
        this.places = places;
    }

    public Collection<Player> getPlayers() {
        return players;
    }

    public void setPlayers(Collection<Player> players) {
        this.players = players;
    }

    /**
     * Create a World. `a' is a reference to the Adventure itself. This reference is
     * used when loading images and sounds.
     *
     * @param a An instance of adventure.
     */
    public World(Adventure a) {
        places = new HashMap<String, Place>();
        players = new LinkedList<Player>();
        owner = a;
    }

    // Every World must implement this method which returns at what
    // Place in the World new Persons are created.
    abstract Place defaultPlace();

    /**
     * Create a Place with name `name' and picture file name "`name'.gif" and
     * add it to this World.
     *
     * @param name A name for the new place and the filename for the gif-image.
     */
    public void createPlace(String name, AdventureActivity target, int pic) {
        createPlace(name, pic, target);
    }

    /**
     * Create a Place with name `name' and picture file name `picture' and add
     * it to this World.
     *
     * @param name    A name for the new place.
     * @param picture The filename of the image associated with the place.
     */
    public void createPlace(String name, int picture, AdventureActivity target) {
        places.put(name, new Place(name, owner.loadPicture(picture, target)));
    }

    /**
     * Returns the Place in this world which has the name `name'.
     *
     * @param name Name of the place to be returned.
     * @return The place in name.
     * @throws NoSuchElementException if matching place does not exist.
     */
    public Place getPlace(String name) {
        Place p = (Place) places.get(name);
        if (p == null)
            throw new NoSuchElementException(name);
        return p;
    }

    /**
     * State that the Player `p' is watching this world.
     *
     * @param p The player to add to the list of players watching this world.
     */
    public void addPlayer(Player p) {
        players.add(p);
    }

    /**
     * Update the control window of any Players who are currently watching
     * `place'.
     *
     * @param place to update.
     */
    public void update(Place place, AdventureActivity target) {
        synchronized (players) {
            Iterator<Player> ls = players.iterator();
            while (ls.hasNext()) {
                Player p = ls.next();
                if (p.currentPlace() == place) {
                    p.update();
                    place.draw(target, ag);
                }
            }
        }
        //place.draw(target, ag);
    }

    /**
     * Say `text' in the Place `place'. The text will become visible at the
     * bottom of the text window of any Players currently watching `place'.
     *
     * @param place The place where the string will be displayed.
     * @param text  The string to be diplayed.
     */
    public void sayAtPlace(Place place, String text, AdventureActivity target) {
        synchronized (players) {
            Iterator<Player> ls = players.iterator();
            while (ls.hasNext()) {
                Player p = ls.next();
                if (p.currentPlace() == place) {
                    p.say(text, target); //FIXME does this always work???
                }
            }
        }
    }

    /**
     * Play `sound' in the Place `place'. The sound will be played by any
     * Players currently watching `place'. Sound can for example be:
     * "songs/what_a_wonderful_world" -- suitable when your key is accepted
     * "effects/gong" -- suitable when Troll kills
     *
     * @param place Place at which the sound will be played.
     * @param sound Filename of the sound.
     */
    public void playAtPlace(Place place, String sound) {
        synchronized (players) {
            Iterator<Player> ls = players.iterator();
            while (ls.hasNext()) {
                Player p = ls.next();
                if (p.currentPlace() == place) {
                    //owner.playSound(sound);
                }
            }
        }
    }
}