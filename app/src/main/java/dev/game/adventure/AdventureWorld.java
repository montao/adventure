package dev.game.adventure;

import android.widget.TextView;

/**
 * Subclass to World that builds up the Adventure World.
 */

public class AdventureWorld extends World {
    // testar commit 2017-12-28 holger

    public String messsage = "HELLO WORLD";

    public AdventureWorld(Adventure a, TextView t, AdventureActivity target, PlaceView ag) {
        super(a);
        setAg(ag);
        // Create all places
        createPlace("Heaven", target, R.mipmap.dungeon2);
        createPlace("Dungeon3", target, R.mipmap.dungeon3);
        createPlace("Dungeon2", target, R.mipmap.dungeon2);
        createPlace("Dungeon", target, R.mipmap.dungeon5);
        createPlace("Dungeon4", target, R.mipmap.dungeon3);

        createPlace("Town", target, R.mipmap.town);
        createPlace("Darkshire", target, R.mipmap.darkshire);

        createPlace("Kulverten", target, R.mipmap.dungeon4);
        createPlace("Dungeon5", target, R.mipmap.dungeon);
        createPlace("Dungeon6", target, R.mipmap.dugeon6);
        createPlace("Dungeon7", target, R.mipmap.dungeon7);
        createPlace("Dungeon8", target, R.mipmap.dungeon8);
        createPlace("Dungeon9", target, R.mipmap.dungeon9);
        createPlace("Dungeon10", target, R.mipmap.dungeon10);
        createPlace("Dungeon11", target, R.mipmap.dungeon11);
        createPlace("Dungeon12", target, R.mipmap.dungeon12);
        createPlace("Dungeon13", target, R.mipmap.dungeon13);
        createPlace("Dungeon14", target, R.mipmap.dungeon14);
        createPlace("Dungeon16", target, R.mipmap.dungeon16);
        createPlace("Dungeon17", target, R.mipmap.dungeon17);
        createPlace("Dungeon18", target, R.mipmap.dungeon17);
        createPlace("Dungeon19", target, R.mipmap.dungeon19);
        createPlace("Dungeon20", target, R.mipmap.dungeon20);

        createPlace("Elfhome", target, R.mipmap.elfhome);
        createPlace("Evilforest", target, R.mipmap.evilforest);
        createPlace("Graveyard", target, R.mipmap.graveyard);

        createPlace("Dungeon21", target, R.mipmap.dungeon21);
        createPlace("Cemetary", target, R.mipmap.cemetery);

        // Create all connections between places
        connect("Dungeon", "Dungeon5", "Down", "Up");
        connect("Dungeon5", "Kulverten", "Down", "Up");
        connect("Dungeon", "Dungeon2", "East", "West");
        connect("Dungeon", "Dungeon3", "North", "South");
        connect("Dungeon", "Town", "North", "South");
        connect("Dungeon5", "Dungeon6", "West", "East");
        connect("Dungeon6", "Dungeon7", "West", "East");
        connect("Town", "Dungeon7", "Down", "Up");
        connect("Dungeon8", "Town", "East", "West");
        connect("Dungeon9", "Town", "West", "East", true, "Magic key");
        connect("Dungeon9", "Dungeon3", "Down", "Up");
        connect("Dungeon10", "Dungeon3", "East", "West");
        connect("Dungeon11", "Dungeon10", "Down", "Up");
        connect("Dungeon11", "Dungeon3", "East", "West");
        connect("Dungeon12", "Dungeon11", "Down", "Up");
        connect("Dungeon12", "Dungeon8", "East", "West");
        connect("Dungeon13", "Dungeon12", "Down", "Up");
        connect("Dungeon13", "Dungeon4", "East", "West");
        connect("Dungeon4", "Dungeon14", "Up", "Down");
        connect("Dungeon14", "Dungeon13", "East", "West");
        connect("Dungeon14", "Dungeon8", "North", "South");
        connect("Dungeon16", "Dungeon14", "East", "West");
        connect("Dungeon16", "Dungeon7", "South", "North");
        connect("Dungeon12", "Dungeon16", "Up", "Down");
        connect("Dungeon17", "Dungeon7", "East", "West");
        connect("Dungeon18", "Dungeon16", "South", "North");
        connect("Dungeon18", "Dungeon19", "Up", "Down");
        connect("Elfhome", "Dungeon17", "Down", "Up");
        connect("Dungeon19", "Elfhome", "East", "West");
        connect("Dungeon20", "Dungeon19", "East", "West");
        connect("Dungeon21", "Dungeon19", "East", "West");
        connect("Cemetary", "Dungeon19", "Up", "Down");
        connect("Cemetary", "Graveyard", "East", "West");

        connect("Dungeon14", "Evilforest", "South", "North");
        connect("Graveyard", "Evilforest", "Up", "Down");
        connect("Graveyard", "Dungeon19", "Down", "Up");
        connect("Graveyard", "Darkshire", "East", "West");
        connect("Dungeon21", "Darkshire", "North", "South");


        getPlace("Town").addThing(new CocaCola("Lukewarm Coca-Cola"));
        getPlace("Town").addThing(new CocaCola("Daft Coca-Cola"));
        getPlace("Town").addThing(new CocaCola("Cold Coca-Cola"));
        getPlace("Town").addThing(new CocaCola("Coca-Cola Light"));
        getPlace("Town").addThing(new CocaCola("Cuba Cola"));
        getPlace("Town").addThing(new Key("Magic key"));

        getPlace("Dungeon2").addThing(new Scroll("Scroll15"));
        getPlace("Dungeon3").addThing(new Key("Key"));

        getPlace("Dungeon3").addThing(new Scroll("Scroll05"));
        getPlace("Dungeon4").addThing(new Scroll("Scroll06"));
        getPlace("Dungeon5").addThing(new Scroll("Scroll07"));
        getPlace("Dungeon6").addThing(new Scroll("Scroll08"));
        getPlace("Dungeon7").addThing(new Scroll("Scroll09"));
        getPlace("Dungeon8").addThing(new Scroll("Scroll10"));
        getPlace("Dungeon9").addThing(new Scroll("Scroll11"));
        getPlace("Dungeon10").addThing(new Scroll("Scroll12"));
        getPlace("Dungeon11").addThing(new Scroll("Scroll13"));
        getPlace("Dungeon12").addThing(new Scroll("Scroll14"));

        getPlace("Graveyard").addThing(new Scroll("Scroll01"));
        getPlace("Graveyard").addThing(new Scroll("Scroll02"));
        getPlace("Graveyard").addThing(new Scroll("Scroll03"));
        getPlace("Graveyard").addThing(new Scroll("Scroll04"));

        getPlace("Darkshire").addThing(new CocaCola("Lukewarm Coca-Cola02"));
        getPlace("Darkshire").addThing(new CocaCola("Daft Coca-Cola02"));
        getPlace("Darkshire").addThing(new CocaCola("Cold Coca-Cola02"));
        getPlace("Darkshire").addThing(new CocaCola("Coca-Cola Light02"));
        getPlace("Darkshire").addThing(new CocaCola("Cuba Cola02"));


        Simulation sim = new Simulation(target);

        // Load images to be used as appearance-parameter for persons
        int robotAppearance = R.mipmap.robot;
        int pirateAppearance = R.mipmap.pirate;
        int skeletonAppearance = R.mipmap.skeletor;
        int zombieAppearance = R.mipmap.brainzombie;
        int goblinAppearance = R.mipmap.goblin;
        // int monsterAppearance = R.mipmap.monster;
        int soldierAppearance = R.mipmap.soldier;
        int heroAppearance = R.mipmap.hero;
        int guardAppearance = R.mipmap.guard;
        int vikingAppearance = R.mipmap.viking;
        int foxAppearance = R.mipmap.fox;
        int ghostAppearance = R.mipmap.ghost;
        int ogreAppearance = R.mipmap.ogre;
        int rockmonsterAppearance = R.mipmap.rockmonster;
        int knightAppearance = R.mipmap.knight;
        int lobGoblinAppearance = R.mipmap.goblinking;
        int raptorAppearance = R.mipmap.raptor;
        int armyjoeAppearance = R.mipmap.armyjoe;
        int arabAppearance = R.mipmap.arab;
        int karenAppearance = R.mipmap.karen;
        int gravelAppearance = R.mipmap.gravel;
        int guard2Appearance = R.mipmap.guard2;
        int chibiknight1Appearance = R.mipmap.chibiknight1;
        int executionerAppearance = R.mipmap.executioner;
        int stonemaneAppearance = R.mipmap.stoneman;
        int thorkwarriorAppearance = R.mipmap.thorkwarrior;
        int mikeAppearance = R.mipmap.mike;
        int joanaAppearance = R.mipmap.joana;
        int ninjaAppearance = R.mipmap.ninja;
        int ninjabaronessAppearance = R.mipmap.ninjabaroness;
        int georgeAppearance = R.mipmap.george;
        int jackthugAppearance = R.mipmap.jackthug;
        int executioner2Appearance = R.mipmap.executioner2;
        int alienbossAppearance = R.mipmap.alienboss;
        int brockAppearance = R.mipmap.brock;
        int chibiknightAppearance = R.mipmap.chibiknight;
        int chinesekingAppearance = R.mipmap.chineseking;
        int chibisamuraiAppearance = R.mipmap.chibisamurai;
        int draculaAppearance = R.mipmap.dracula;
        int purpleskinalienAppearance = R.mipmap.purpleskinalien;
        //int zombiefarmerAppearance = R.mipmap.zombiefarmer;
        int androidbossAppearance = R.mipmap.androidboss;
        int warbotAppearance = R.mipmap.warbot;
        int executioner3Appearance = R.mipmap.executioner3;
        int redbotAppearance = R.mipmap.redbot;


        // --- Add new persons here ---
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Stoneman", stonemaneAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Thorkwarrior", thorkwarriorAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "ChibiKnight01", chibiknight1Appearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Executioner", executionerAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Karen", karenAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Gravel", gravelAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "DungeonGuard", guard2Appearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Goblin", goblinAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Skeleton", skeletonAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Robot", robotAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Zombie", zombieAppearance, target);
        //   new WalkingPerson(sim, this, "Monster", monsterAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Soldier", soldierAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Hero", heroAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Guard", guardAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Viking", vikingAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Fox", foxAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Ghost", ghostAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Ogre", ogreAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Rockmonster", rockmonsterAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Knight", knightAppearance, target);
        //  new WalkingPerson(sim, this, "Iobgoblin", lobGoblinAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Raptor", raptorAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Army Joe", armyjoeAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Arab", arabAppearance, target);

        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Mike", mikeAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "George", georgeAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Alien Boss", alienbossAppearance, target);
        // new WalkingPerson(sim, this, "Executioner3", executioner3Appearance, target);

        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Brock", brockAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Chibi Knight", chibiknightAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "King", chinesekingAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Samurai", chibisamuraiAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Dracula", draculaAppearance, target);

        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Joana", joanaAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Android Boss", androidbossAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "RedBot", redbotAppearance, target);

        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Ninja", ninjaAppearance, target);

        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Ninja Baroness", ninjabaronessAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Jackthug", jackthugAppearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Executioner02", executioner2Appearance, target);
        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Purpleskin", purpleskinalienAppearance, target);

        if (Math.random() > 0.2)
            new WalkingPerson(sim, this, "Warbot", warbotAppearance, target);

        new Troll(sim, this, "Troll", target);
    }

    /**
     * The place where persons are placed by default
     *
     * @return The default place.
     */
    public Place defaultPlace() {
        return randomPlace();//
        // return getPlace( "Dungeon" );
    }

    /**
     * A random place where persons are placed by default
     *
     * @return A random place.
     */
    public Place randomPlace() {

        String[] places = {"Heaven", "Dungeon3", "Dungeon2", "Dungeon", "Dungeon4", "Dungeon5", "Dungeon10", "Dungeon11", "Dungeon12", "Dungeon13", "Dungeon14", "Dungeon16", "Dungeon17", "Dungeon18", "Dungeon19", "Dungeon20", "Dungeon21", "Darkshire", "Elfhome", "Evilforest", "Graveyard", "Cemetary","Dungeon6", "Dungeon7", "Dungeon8", "Dungeon9", "Town",
                "Kulverten"};

        return getPlace((String) (places[((int) (Math.random() * places.length))]));
    }

    public void connect(String p1, String p2, String door1, String door2) {
        Place place1 = getPlace(p1);
        Place place2 = getPlace(p2);
        place1.addExit(door1, place2);
        place2.addExit(door2, place1);
    }

    // overload previous method
    public void connect(String p1, String p2, String door1, String door2, boolean lock) {
        Place place1 = getPlace(p1);
        Place place2 = getPlace(p2);
        place1.addExit(door1, place2, lock);
        place2.addExit(door2, place1, lock);
    }

    // overload previous method
    public void connect(String p1, String p2, String door1, String door2, boolean lock, String keyReq) {
        Place place1 = getPlace(p1);
        Place place2 = getPlace(p2);
        place1.addExit(door1, place2, lock, keyReq);
        place2.addExit(door2, place1, lock, keyReq);
    }

    }