package dev.game.adventure;

public interface Wakeable {
    void wakeup();
}