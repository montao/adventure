package dev.game.adventure;

import android.net.wifi.p2p.WifiP2pManager;

import java.util.*;


/*
 * Used by player to enable control panel for movement
 */
public class Commands  {
	private static final long serialVersionUID = 100L;
	private Person person;
	private List inventory;
	private List thingsAtPlace;
//	private JPanel buttons;

	/**
	 * 
	 * @param who
	 *            A reference to the Person reflected by the command panel.
	 * 
	 */
	public Commands(Person who) {
//		setLayout(new BorderLayout());
		this.person = who;

//		buttons = new JPanel();
//		buttons.setLayout(new GridLayout(4, 4));
	//	addNoButton();
		addButton("North");

	//	addNoButton();
		addButton("Up");
		addButton("West");
	//	addNoButton();
		addButton("East");
	//	addNoButton();
	//	addNoButton();
		addButton("South");
	//	addNoButton();
		addButton("Down");
		addButton("Drop");
		addButton("Take");
		addButton("Talk");

//		thingsAtPlace = new java.awt.List(10);
//		add("West", thingsAtPlace);

//		add("Center", buttons);

//		inventory = new java.awt.List(10);
//		add("East", inventory);

		updateLists();

	}

	private void addButton(String command) {
//		JButton b = new JButton(command);
//		buttons.add(b);
		// component = buttons.getComponent(n)
		// list.add(component)
//		b.addActionListener(this);
	}

/*	private void addNoButton() {
		buttons.add(new JPanel());
	}

	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();
		if (cmd.equals("Take")) {
			person.grab(thingsAtPlace.getSelectedItem());
		} else if (cmd.equals("Drop")) {
			person.drop(inventory.getSelectedItem());
		} else if (cmd.equals("Talk")) {
			person.place.query(person);
		} else {
			person.go(cmd);
		}
	}
*/
	/**
	 * 
	 * Updates the things lists.
	 * 
	 */
	public void updateLists() {

	//	String selected = inventory.getSelectedItem();
	//	while (inventory.getItemCount() > 0)
	//		inventory.remove(0);

		synchronized (person.things) {
			Iterator<Thing> iter = person.things.iterator();
			while (iter.hasNext()) {
				Thing thing = iter.next();
//				inventory.add(thing.name);
	//			if (thing.name.equals(selected)) {
	//				inventory.select(inventory.getItemCount() - 1);
				}
			}
		}

	//	selected = thingsAtPlace.getSelectedItem();

	//	while (thingsAtPlace.getItemCount() > 0)
	//		thingsAtPlace.remove(0);

	/*	synchronized (person.place.things) {
			Iterator<Thing> thingIter = person.place.things.iterator();
			while (thingIter.hasNext()) {
				Thing thing = thingIter.next();
				thingsAtPlace.add(thing.name);
				if (thing.name.equals(selected)) {
					thingsAtPlace.select(inventory.getItemCount() - 1);
				}
			}
		}

	}
*/
}
