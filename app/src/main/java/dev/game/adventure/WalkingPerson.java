package dev.game.adventure;

import java.util.*;

public class WalkingPerson extends Person implements Wakeable {

    private String[] msgs = {
            "The horrible shrieks of the undead chill your bones.",
            "The maddening horrors of this place make your skin crawl.",
            "Death fills the very air of everywhere you turn.",
            "Frightening voices in the air whisper to you, imploring you to join their ranks.",
            "The pungent stength of lurking horrors fills your nostrils.",
            "Morbid visions glimpse before eyes.",
            "Howling screams of a distant Leviathan far below shiver your sanity and prophesize your doom.",
            "Ancient abominations and unspeakable horrors stir in their sleep as you sneak them by."};

    public Simulation s;
    private Place lastPlaceofKey;

    WalkingPerson(Simulation s, World world, String name, int app, AdventureActivity target) {
        super(world, name, app, target);
        this.s = s;
        s.wakeMeAfter(this, (1000 + Math.random() * 10000));
    }

    public void wakeup() {

        for (Iterator<Thing> i = place.things.iterator(); i.hasNext(); ) {
            if ((i.next()).name.equals("Key"))
                lastPlaceofKey = place;
        }

        if (Math.random() > 0.2) {
            if (place.pickThingName() != null) {
                grab(place.pickThingName(), target);
            }
        }

        if (Math.random() < 0.4) {
            if (things.size() != 0) {
                drop(((Thing) (things.toArray()[((int) (Math.random() * things
                        .size()))])).name, target);
            }
        }

        go(place.randomDoor(), target);
        s.wakeMeAfter(this, (8000 + Math.random() * 10000));
    }

    public void query(Person questioner, AdventureActivity target) {
        if (lastPlaceofKey != null) {
            if (findThing("Key") != null)
                say("I've got the key!", target);
            else
                say("I've seen the key in " + lastPlaceofKey.name, target);
        } else
            say(msgs[(int) (Math.random() * msgs.length)], target);
    }

    public void attack(Person attacker, AdventureActivity target) {
        if (lastPlaceofKey != null) {
            if (findThing("Key") != null)
                drop("Key", target);
            else
                say("I've seen the key in " + lastPlaceofKey.name, target);
        } else
            //say("Len no!", target);
            say(msgs[(int) (Math.random() * msgs.length)], target);
    }

}