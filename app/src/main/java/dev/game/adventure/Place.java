package dev.game.adventure;

import android.graphics.drawable.Drawable;
import android.widget.Toast;

import java.util.*;

/**
 * ADT for places. Every Place object keeps information about which persons and
 * objects that are there and how to go to other places
 */
public class Place {

    private String[] msgs = {
            "The horrible shrieks of the undead chill your bones.",
            "The maddening horrors of this place make your skin crawl.",
            "Death fills the very air of everywhere you turn.",
            "Frightening voices in the air whisper to you, imploring you to join their ranks.",
            "The pungent stength of lurking horrors fills your nostrils.",
            "Morbid visions glimpse before eyes.",
            "Howling screams of a distant Leviathan far below shiver your sanity and prophesize your doom.",
            "Ancient abominations and unspeakable horrors stir in their sleep as you sneak them by."};
    /**
     * Name of this Place.
     */
    public String name = "New place";
    //Drawable appearance = getResources().getDrawable(R.mipmap.view1)
    /**
     * Maps doornames to neighboring places.
     */
    public Map<String, Place> exits;

    /**
     * Keeps record of locked doors in different directions.
     */
    public Map<String, Boolean> locked;

    /**
     * Keeps record of specific key requirement, if any, for each door.
     */
    public Map<String, String> keyRequirements;

    /**
     * Persons at this Place.
     */
    public Collection<Person> persons;

    /**
     * Things at this Place.
     */
    public Collection<Thing> things;

    /**
     * Image of this Place.
     */
    public Drawable image;

    /**
     * Create Place named name with associated image.
     *
     * @param name    Name of this Place.
     * @param picture a 320 x 240 pixel image which will be used when displaying the
     *                place in the Player control window.
     */
    public Place(String name, Drawable picture) {
        this.name = name;
        exits = new HashMap<String, Place>();
        persons = Collections.synchronizedCollection(new LinkedList<Person>());
        things = Collections.synchronizedCollection(new LinkedList<Thing>());
        image = picture;
        locked = new HashMap<String, Boolean>();
        keyRequirements = new HashMap<String, String>();
    }

    /**
     * Add a door leading from this Place to another.
     *
     * @param doorName Can be any of "North", "East", "South", "West", "Up",
     *                 or, "Down".
     * @param whereto  The Place to which the new door will lead.
     */
    public void addExit(String doorName, Place whereto) {
        exits.put(doorName, whereto);
        locked.put(doorName, false); // doors are unlocked by default
        keyRequirements.put(doorName, null); // no key requirement unless specified
    }
    /**
     * Overloads the above method.
     *
     * @param lock  True or false depending on whether the passage is locked.
     */
    public void addExit(String doorName, Place whereto, boolean lock) {
        exits.put(doorName, whereto);
        locked.put(doorName, lock);
        keyRequirements.put(doorName, null);
    }

    /**
     * Overloads the above method.
     *
     * @param keyReq  specifies key requirement.
     */
    public void addExit(String doorName, Place whereto, boolean lock, String keyReq) {
        exits.put(doorName, whereto);
        locked.put(doorName, lock);
        keyRequirements.put(doorName, keyReq);
    }

    /**
     * Return the Place which doorName leads to.
     *
     * @param doorName Name of a door. Can be any of "North", "East", "South",
     * @return The Place lead to by doorName.
     */
    public Place getExit(String doorName) {
        return exits.get(doorName);
    }

    /**
     * Randomly select and return the name of any door at this place.
     *
     * @return A random door; null if no doors exist.
     */
    public String randomDoor() {
        if (exits.isEmpty())
            return null;

        Object[] doorNames = exits.keySet().toArray();

        int i = (int) (Math.random() * doorNames.length);

        return (String) doorNames[i];
    }

    /**
     * Draw the place, as well as the people present.
     * <p>
     * <p>
     * A graphics context on which to draw the Place. To do: avoid
     * collosion when a Place gets crowded with collision detection
     * algorithm
     */
    public void draw(AdventureActivity target, PlaceView ag) {
        ag.setPersons(persons);
        if (this.getName().equals("Heaven")) {
            final Toast toast = Toast.makeText(target.ag.getContext(), "GAME OVER!\n", Toast.LENGTH_LONG);// duration);
            toast.show();

        }

    }

    /**
     * Add a Person to this Place.
     *
     * @param person A Person to add to the persons collection.
     */
    public void enter(Person person, AdventureActivity target) {

        persons.add(person);
        // get all the exits
        Object[] doorNames = this.exits.keySet().toArray();
        String s = "";
        int i = 1;
        // build up the string with info about exits. to do: do this with
        // stringbuilder / stringbuffer
        for (Object obj : doorNames) {
            if (i < doorNames.length) {
                s = s + obj.toString().toLowerCase();

                if (i < doorNames.length) {

                    s = s + ",";
                }

            }
            if (i == doorNames.length && i > 1) {
                s = s + " and " + obj.toString().toLowerCase();
            }
            if (i == doorNames.length && i == 1) {
                s = obj.toString().toLowerCase();
            }
            ++i;
        }
        // check if this is you
        if (person.getName().equals("You")) {



            String rand = msgs[(int) (Math.random() * msgs.length)];
            //person.getWorld().sayAtPlace(person.place, rand, target);
            // don't write out exit info if there is none
            if (!this.name.equals("Heaven")) {
                person.getWorld().sayAtPlace(person.place,
                        "There are exits " + s + ".", target);
            }
        }
        if (!person.getName().equals("You")) {
            person.getWorld().sayAtPlace(this, person.getName() + " appears in " + this.getName() + ".", target);

            if(Math.random() > 0.4)
                person.getWorld().sayAtPlace(this, person.getName() + " says: " + msgs[(int) (Math.random() * msgs.length)], target);

        } else {
            person.getWorld().sayAtPlace(this, person.getName() + " appear in " + this.getName() + ".", target);

        }
    }

    /**
     * Remove a Person from this Place.
     *
     * @param person The Person to remove from the persons collection.
     */
    public void exit(Person person) {
        persons.remove(person);
    }

    /**
     * Add a Thing to this Place.
     *
     * @param thing A Thing to add to the things collection.
     */
    public void addThing(Thing thing) {
        things.add(thing);
    }

    /**
     * Remove a Thing named `thingName' from this place.
     *
     * @param thingName A name referring to a Thing that is to be removed from the
     *                  things collection.
     * @return The Thing to be removed; null if no matchin Thing was found in
     * the things collection.
     */
    public Thing removeThing(String thingName) {
        Thing item = null;
        synchronized (things) {
            Iterator<Thing> iterate = things.iterator();
            while (iterate.hasNext()) {
                Thing t = iterate.next();
                if (t.name.equals(thingName)) {
                    item = t;
                    break;
                }
            }
        }

        if (item != null) {
            things.remove(item);
        }

        return item;
    }

    /**
     * Randomly return one of the things at this Place.
     *
     * @return The name of a random Thing at this Place.
     */
    public String pickThingName() {
        if (things.isEmpty())
            return null; // Nothing here

        Object[] allThings = things.toArray();
        Thing it = (Thing) allThings[(int) (Math.random() * allThings.length)];

        return it.name;
    }

    /**
     * Query each of the Persons in this Place, except the questioner, by
     * calling the respective query-method, passing 'questioner' as its
     * parameter.
     *
     * @param questioner The Person querying, passed to each query-call.
     */
    public void query(Person questioner) {
        synchronized (persons) {
            Iterator<Person> iterate = persons.iterator();
            while (iterate.hasNext()) {
                Person person = iterate.next();
                if (person != questioner)
                    person.query(questioner);
            }
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Place> getExits() {
        return exits;
    }

    public void setExits(Map<String, Place> exits) {
        this.exits = exits;
    }

    public Collection<Person> getPersons() {
        return persons;
    }

    public void setPersons(Collection<Person> persons) {
        this.persons = persons;
    }

    public Collection<Thing> getThings() {
        return things;
    }

    public void setThings(Collection<Thing> things) {
        this.things = things;
    }

    public Drawable getImage() {
        return image;
    }

    public void setImage(Drawable image) {
        this.image = image;
    }
}
