package dev.game.adventure;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collection;
import java.util.Iterator;

/**
 * Created by developer on 03/01/2018.
 */

public class PlaceView extends View {

    /**
     * Persons at this PlaceView.
     */
    public Collection<Person> persons;
    Bitmap bitmap;
    public Collection<Person> getPersons() {
        return persons;
    }

    Person mainCharacter;

    Context cont;
    TextView text;
    AdventureActivity target;

    public PlaceView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        target = (AdventureActivity) context;
        this.cont = context;
        int resourceIdHero = 0;
        bitmap = BitmapFactory.decodeResource(getResources(), resourceIdHero); //load a bitmap image
    }

    public void setPersons(Collection<Person> persons) {
        this.persons = persons;
    }

    public void display(String s1, AdventureActivity target, Place p) {
        setBackground(p.image);
        target.scrollable.append("This place is called " + p.getName() + ".");
        if(p.getName().equals("Heaven")) {
            final Toast toast = Toast.makeText(target.ag.getContext(), "GAME OVER!\n", Toast.LENGTH_LONG);// duration);
            toast.show();
        }

    }


    // update the canvas in order to display the game action
    @Override
    public void onDraw(Canvas canvas) {

        super.onDraw(canvas);
        int xx = 200;
        int yy = 0;
        if (persons != null) {
            synchronized (persons) {
                Iterator<Person> iterate = persons.iterator();
                while (iterate.hasNext()) {
                    Person p = iterate.next();
                    if (p.getImage() != 0) {
                        bitmap = BitmapFactory.decodeResource(getResources(), p.getImage()); //load a character image
                        // Draw the visible person's appearance
                        if(xx > canvas.getWidth())
                            xx = 0;
                        canvas.drawBitmap(bitmap, xx , canvas.getHeight()- bitmap.getHeight() , null);
                        // Draw the name
                        Paint paint = new Paint();
                        paint.setStyle(Paint.Style.FILL);
                        canvas.save();
                        paint.setStrokeWidth(1);
                        paint.setColor(Color.WHITE);
                        paint.setTextSize(50);
                        canvas.drawText(p.name, (float)(xx+0.25*bitmap.getWidth()), (float) (canvas.getHeight() ), paint);
                        xx += bitmap.getWidth()*0.75;
                    }
                }
            }
        }
        canvas.save(); //Save the position of the canvas.
        canvas.restore();
        //Call the next frame.
        invalidate();
    }
}
