package dev.game.adventure;

import java.util.*;

/**
 * ADT for persons which is completed which subclasses to creating actors with
 * specific properties
 */
public class Person {

    /**
     * Name of the Person.
     */
    public String name;


    /**
     * The World which this Person is associated with.
     */
    public World world;

    /**
     * Tells where this Person is at the moment.
     */
    public Place place;

    /**
     * The inventory, contains Things.
     */
    public Collection<Thing> things;
    AdventureActivity target;
    /**
     * Shows how the Person looks like
     */
    public int image;


    /**
     * Create Person named `name' in world `world'.
     *
     * @param world The world where the Person is created.
     * @param name  Name of the Person.
     * @param app   An image used to display the Person.
     */
    public Person(World world, String name, int app, AdventureActivity target) {
        this.world = world;
        this.name = name;
        this.image = app;
        this.target = target;
        place = world.defaultPlace();
        if (this.getName().equals("Skeleton")) {
            world.sayAtPlace(place,
                    "A terrifying skeleton warrior appears!", target);
        }
        place.enter(this, target);
        things = Collections.synchronizedCollection(new LinkedList<Thing>());
    }

    /**
     * Go directly, and quietly, to 'place'. That is to say, without posting a
     * message that you're doing so.
     *
     * @param place A string referring to the Place to go to.
     * @see #goTo(Place, AdventureActivity)
     * @see #go
     */
    public void goTo(String place, AdventureActivity target) {
        goTo(world.getPlace(place), target);
    }

    /**
     * Go directly, and quietly, to `whereTo'. That is to say, without posting a
     * message that you're doing so.
     *
     * @param whereTo The Place to go to. Can be null in which case nothing happens.
     * @see #goTo(Place, AdventureActivity
     * @see #go
     */
    public void goTo(Place whereTo, AdventureActivity target) {
        if (whereTo != null) {
            place.exit(this);
            whereTo.enter(this, target);

            if (this.getName().equals("Skeleton")) {
              //  world.sayAtPlace(whereTo,
                //        "A terrifying skeleton warrior appears!", target);
            }

            world.update(place, target);
            // Record our new position.
            place = whereTo;
            // Also update Player's here.
            world.update(place, target);
        }
    }

    /**
     * Go through the door `door', verbosly. That is to say, post a message that
     * you re doing so. Don't complain if the door doesn't exist.
     *
     * @param door Name of the door to exit through.
     //* @see #goTo(String)
     * @see #goTo(Place, AdventureActivity
     */
    public void go(String door, AdventureActivity target) {
        Place whereTo = place.getExit(door);
        boolean success = false;

        if (whereTo != null) {
            if (!name.equals("You") && !place.locked.get(door)) {
                success = true;
                world.sayAtPlace(place, name + " goes " + door, target);
            }
            if (name.equals("You")) {
                if (!place.locked.get(door)) {
                    success = true;
                }
                else { // door is locked
                    for (Thing thing : things) {
                        if (thing instanceof Key) {
                            String keyReq = place.keyRequirements.get(door);
                            if (keyReq == null || thing.name.equals(keyReq)) {
                                place.locked.put(door, false);
                                world.sayAtPlace(place, "You have now unlocked door " + door + ". ", target);
                                success = true;
                                break;
                            }
                        }
                    }
                }
                if (success) {
                    world.sayAtPlace(place, name + " are going " + door, target);
                } else {
                    String keyReq = place.keyRequirements.get(door);
                    if (keyReq != null) {
                        world.sayAtPlace(place, "Door " + door + " can only be opened with " + keyReq + ". ", target);
                    }
                    else {
                        world.sayAtPlace(place, "Door " + door + " is locked. ", target);
                    }
                }
            }
            if (success) {
                goTo(whereTo, target);
            }
        }


    }

    /**
     * Print `text' in the bottom of the text window. Prepend it with a cue
     * indicating who is speaking. The text will only show up for Players at the
     * same Place as the one who is speaking.
     *
     * @param text String to be displayed.
     */
    public void say(String text, AdventureActivity target) {
        String verb = " says ";
        if (name.equals("You"))
            verb = " say ";

        world.sayAtPlace(place, name + verb + text, target);
    }

    /**
     * Grab a Thing from this Place and place it in the inventory.
     *
     * @param thingName Name referring to a Thing to be grabbed.
     * @see #drop
     */
    public synchronized void grab(String thingName, AdventureActivity target) {
        Thing item = place.removeThing(thingName);
        if (item != null) {
            things.add(item);
            world.sayAtPlace(place, name + " is taking " + thingName, target);
            world.update(place, target);

            // How to win the game. Bring the key to the town
            if (place.getName().equals("Town") && name.equals("You") && thingName.equals("Key")) {
                world.sayAtPlace(place, "-- Congratulations! --", target);
            }

        }
    }
    // overloaded method
    public synchronized void grab(AdventureActivity target) {
        List<Thing> found = new ArrayList<Thing>();
        for (Thing thing : place.things) {
            String thingName = thing.name;
            //Thing item = place.removeThing(thingName); CRASHES!!
            things.add(thing);
            found.add(thing);
            world.sayAtPlace(place, name + " is taking " + thingName, target);
            world.update(place, target);

            // How to win the game. Bring the key to the town
            if (place.getName().equals("Town") && name.equals("You") && thingName.equals("Key")) {
                world.sayAtPlace(place, "-- Congratulations! --", target);
            }


        }
        place.things.removeAll(found); // use instead of method removeThing
    }

    /**
     * Drop a Thing the ground by deleting it from the inventory.
     *
     * @param thingName Name referring to a Thing to be dropped.
     * @see #grab
     */
    public synchronized void drop(String thingName, AdventureActivity target) {
        Thing item = findThing(thingName);

        if (item != null) {
            things.remove(item);
            place.addThing(item);
            world.sayAtPlace(place, name + " is dropping " + thingName, target);
            world.update(place, target);
            if (place.getName().equals("Town") && name.equals("You") && thingName.equals("Key")) {
                world.sayAtPlace(place, "-- Congratulations! --", target);
            }
        }



    }

    /**
     * Find a Thing in our inventory.
     *
     * @param thingName The name of a thing to find.
     */
    public Thing findThing(String thingName) {
        synchronized (things) {
            Iterator<Thing> iterate = things.iterator();
            while (iterate.hasNext()) {
                Thing t = (Thing) iterate.next();
                if (t.name.equals(thingName))
                    return t;
            }
        }
        return null;
    }

    /**
     * Respond to a query from another person.
     *
     * @param questioner The Person querying.
     */
    public void query(Person questioner) {
        // Default case - don't say anything
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public Collection<Thing> getThings() {
        return things;
    }

    public void setThings(Collection<Thing> things) {
        this.things = things;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
