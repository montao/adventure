package dev.game.adventure;

import android.widget.TextView;

/**
 * A Player object enables commands to control a certain person: "you". This
 * object is displayed graphically as a control panel where the user both can
 * control and follow the course of events
 */
class Player {
    public Person getMe() {
        return me;
    }

    public void setMe(Person me) {
        this.me = me;
    }

    private Person me;
    private Commands commands;
    private TextView textView;

    public String toString() {
        return getMe().getName();
    }
    /**
     * Creates a new instance of Player, in World w, reflecting Person p.
     *
     * @param w The world in which the Player will play in.
     * @param p The Person associated by Player.
     */
    Player(World w, Person p, TextView t) {
        me = p;
        commands = new Commands(me);
        textView = t;
      //  t.append("You are in a dungeon. The horrible shrieks of the undead chill your bones.");
        w.addPlayer(this);
    }

    /**
     * Update the players graphical interface.
     */
    public void update() {
        commands.updateLists();
    }

    /**
     * Display a string in the players graphical interface.
     *
     * @param text A string to display.
     */
    void say(final String text, final AdventureActivity target) {
        //target.runOnUiThread(new Runnable() {
          //  @Override
           // public void run() {
                //stuff that updates ui
                //FIXME Why doesn't this always work?
                target.scrollable.append(text + '\n');
       //     }
        //});
    }

    /**
     * Returns the Place of the Person associated with the Player.
     *
     * @return The Place of the Person associated with the Player.
     */
    public Place currentPlace() {
        return me.place;
    }
}
