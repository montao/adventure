package dev.game.adventure;

import android.os.Handler;
import android.widget.Toast;

import java.util.*;

public class Troll extends WalkingPerson {

    private boolean hasCoke = false;
    private AdventureActivity target;

    Troll(Simulation s, World world, String name, AdventureActivity app) {
        super(s, world, name, R.mipmap.lobgoblin, app);
        this.target = app;
        goTo("Kulverten", target);
    }

    public void wakeup() {

        Person victim = (Person) (place.persons.toArray()[((int) (Math.random() * place.persons
                .size()))]);


        if (victim instanceof Troll) { // Don't commit suicide
        } else {
            say("Victim is " + victim.getName() + ".", target);
            Thing thing;

            for (Iterator<Thing> i = victim.things.iterator(); i.hasNext(); ) {

                thing = i.next();

                if (thing instanceof CocaCola) {
                    hasCoke = true;
                    //victim.things.remove(thing);
                    i.remove();
                    world.update(place, target);
                    say("Now I take your Coca Cola!", target);
                    //TODO: Actually take the drink
                } else {
                    drop(thing.name, target); // drop all items so that the key doesn't end up in heaven
                }
            }

            if (!hasCoke) {
                say("Now I kill you!", target); // TODO: Take all the items of the victim
                //  this.getWorld().playAtPlace(place, "effects/gong");
                final Person victim2 = victim;
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        victim2.goTo("Heaven", target);
                        world.update(victim2.place, target);
                        world.sayAtPlace(victim2.place, "-- Game Over --", target);
                        if (victim2.getName().equals("You")) {
                            target.ag.setBackground(world.getPlace("Heaven").getImage());
                            final Toast toast = Toast.makeText(target.ag.getContext(), "GAME OVER!\n", Toast.LENGTH_LONG);// duration);
                            toast.show();
                        }

                    }
                }, 1500);
            }
        }
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(Math.random() > 0.3)
                    go(place.randomDoor(), target);
            }
        }, 2500);
        s.wakeMeAfter(this, (80 + Math.random() * 1000));
    }
}
