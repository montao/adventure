package dev.game.adventure;

public class Simulation extends Thread {

    private PriorityQueue prioQueue;
    AdventureActivity target;

    Simulation(AdventureActivity target) {
        prioQueue = new PriorityQueue();
        this.target = target;
        start();
    }
    public String getHelloWorldString(){return "HELLO WORLD";}

    void wakeMeAfter(Wakeable sleepingObject, double time) {
        prioQueue.enqueue(sleepingObject, System.currentTimeMillis() + time);
    }

    public void run() {
        for (; ; ) {
            try {
                sleep(5000);
                if (prioQueue.getFirstTime() <= System.currentTimeMillis()) {

                    target.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            ((Wakeable) prioQueue.getFirst()).wakeup();
                            prioQueue.dequeue();
                        }
                    });
                }
            } catch (InterruptedException e) {
            }
        }
    }
}
