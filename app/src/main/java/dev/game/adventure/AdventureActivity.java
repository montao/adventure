package dev.game.adventure;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.*;
import android.support.annotation.Nullable;
import android.os.*;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.AttributeSet;
import android.view.*;
import android.widget.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;


/**
 * An adventure that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class AdventureActivity extends AppCompatActivity {
    /**
     * Whether or not the system UI should be auto-hidden after
     * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
     */
    private static final boolean AUTO_HIDE = true;

    public PlaceView getAg() {
        return ag;
    }

    public void setAg(PlaceView ag) {
        this.ag = ag;
    }

    PlaceView ag;

    /**
     * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
     * user interaction before hiding the system UI.
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Some older devices needs a small delay between UI widget updates
     * and a change of the status and navigation bar.
     */
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private View mContentView;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private View mControlsView;
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
            mControlsView.setVisibility(View.VISIBLE);
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
    /**
     * Touch listener to use for in-layout UI controls to delay hiding the
     * system UI. This is to prevent the jarring behavior of controls going away
     * while interacting with activity UI.
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };
    private BottomSheetBehavior mBottomSheetBehavior1;
    private boolean grab = true;
    TextView scrollable;
    Collection<Thing> col;
    private ListView mListView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);
        scrollable = (TextView) findViewById(R.id.textView1);
        scrollable.setMovementMethod(new ScrollingMovementMethod());
        ag = findViewById(R.id.view);
        ag.text = scrollable;
        ag = findViewById(R.id.view);
        mListView = (ListView) findViewById(R.id.recipe_list_view);
        final Adventure adventure = new Adventure(scrollable, this, ag);
        Button buttonOne = (Button) findViewById(R.id.close);
        final Context c = this;

        buttonOne.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                ag.mainCharacter.go("North", ag.target);
                ag.display("North", ag.target, ag.mainCharacter.place);
                col = adventure.getPlayer().currentPlace().getThings();
                final ArrayList<String> list = new ArrayList<String>();
                for (Iterator<Thing> iterator = col.iterator(); iterator.hasNext(); ) {
                    Thing thing = iterator.next();
                    list.add(thing.name);
                }


                ArrayAdapter adapter2 = new ArrayAdapter(c, android.R.layout.simple_list_item_1, list);
                mListView.setAdapter(adapter2);
            }
        });

        Button buttonOne2 = (Button) findViewById(R.id.close2);
        buttonOne2.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                ag.mainCharacter.go("East", ag.target);
                ag.display("East", ag.target, ag.mainCharacter.place);
                col = adventure.getPlayer().currentPlace().getThings();
                final ArrayList<String> list = new ArrayList<String>();
                for (Iterator<Thing> iterator = col.iterator(); iterator.hasNext(); ) {
                    Thing thing = iterator.next();
                    list.add(thing.name);
                }


                ArrayAdapter adapter2 = new ArrayAdapter(c, android.R.layout.simple_list_item_1, list);
                mListView.setAdapter(adapter2);
            }
        });

        Button buttonOne3 = (Button) findViewById(R.id.close3);
        buttonOne3.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                ag.mainCharacter.go("South", ag.target);
                ag.display("South", ag.target, ag.mainCharacter.place);
                col = adventure.getPlayer().currentPlace().getThings();
                final ArrayList<String> list = new ArrayList<String>();
                for (Iterator<Thing> iterator = col.iterator(); iterator.hasNext(); ) {
                    Thing thing = iterator.next();
                    list.add(thing.name);
                }


                ArrayAdapter adapter2 = new ArrayAdapter(c, android.R.layout.simple_list_item_1, list);
                mListView.setAdapter(adapter2);
            }
        });

        Button buttonOne4 = (Button) findViewById(R.id.close4);
        buttonOne4.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                ag.mainCharacter.go("West", ag.target);
                ag.display("West", ag.target, ag.mainCharacter.place);
                col = adventure.getPlayer().currentPlace().getThings();
                final ArrayList<String> list = new ArrayList<String>();
                for (Iterator<Thing> iterator = col.iterator(); iterator.hasNext(); ) {
                    Thing thing = iterator.next();
                    list.add(thing.name);
                }


                ArrayAdapter adapter2 = new ArrayAdapter(c, android.R.layout.simple_list_item_1, list);
                mListView.setAdapter(adapter2);
            }
        });

        Button buttonOne5 = (Button) findViewById(R.id.close5);
        buttonOne5.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                ag.mainCharacter.go("Up", ag.target);
                ag.display("Up", ag.target, ag.mainCharacter.place);
                col = adventure.getPlayer().currentPlace().getThings();
            }
        });

        Button buttonOne6 = (Button) findViewById(R.id.close6);
        buttonOne6.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                ag.mainCharacter.go("Down", ag.target);
                ag.display("Down", ag.target, ag.mainCharacter.place);
                col = adventure.getPlayer().currentPlace().getThings();
            }
        });

        Button buttonOne7 = (Button) findViewById(R.id.close7);
        buttonOne7.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {
                ag.mainCharacter.say("Where is the key?", ag.target);

                for (Iterator<Person> i = ag.persons.iterator(); i.hasNext(); ) {
                    Person item = i.next();
                    if (item instanceof WalkingPerson) {
                        WalkingPerson item2 = (WalkingPerson) item;
                        item2.query(ag.mainCharacter, ag.target);
                    }
                }
            }
        });


        Button buttonOne8 = (Button) findViewById(R.id.close8);
        buttonOne8.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {

                ag.mainCharacter.say("Now I attack you.", ag.target);

                for (Iterator<Person> i = ag.persons.iterator(); i.hasNext(); ) {
                    Person item = i.next();
                    if (item instanceof WalkingPerson) {
                        WalkingPerson item2 = (WalkingPerson) item;
                        item2.attack(ag.mainCharacter, ag.target);
                    }
                }


            }
        });

        ag.mainCharacter.goTo("Town", this); // make the player start in town
        col = adventure.getPlayer().currentPlace().getThings(); // get all things at the current place

        final ArrayList<String> list = new ArrayList<String>();
        for (Iterator<Thing> iterator = col.iterator(); iterator.hasNext(); ) {
            Thing thing = iterator.next();
            list.add(thing.name);
        }

        ArrayAdapter adapter2 = new ArrayAdapter(this, android.R.layout.simple_list_item_1, list);
        mListView.setAdapter(adapter2);


        final View bottomSheet = findViewById(R.id.bottom_sheet);
        mBottomSheetBehavior1 = BottomSheetBehavior.from(bottomSheet);

        final Button mButton1 = (Button) findViewById(R.id.close9);
        final Context c4 = this;
        mButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                grab = true;
                col = adventure.getPlayer().currentPlace().getThings(); // get all things at the current place

                final ArrayList<String> list = new ArrayList<String>();
                for (Iterator<Thing> iterator = col.iterator(); iterator.hasNext(); ) {
                    Thing thing = iterator.next();
                    list.add(thing.name);
                }

                ArrayAdapter adapter2 = new ArrayAdapter(c4, android.R.layout.simple_list_item_1, list);
                mListView.setAdapter(adapter2);

                if (mBottomSheetBehavior1.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheet.setVisibility(View.VISIBLE);
                    mBottomSheetBehavior1.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    mBottomSheetBehavior1.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    bottomSheet.setVisibility(View.GONE);
                }
            }
        });

        ag.display("", this, ag.mainCharacter.place);

        // Set an item click listener for ListView
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Get the selected item text from ListView
                String selectedItem = (String) parent.getItemAtPosition(position);
                final ArrayList<String> list = new ArrayList<String>();

                if (grab) {
                    ag.mainCharacter.grab(selectedItem, ag.target);
                    col = adventure.getPlayer().currentPlace().getThings();
                    for (Iterator<Thing> iterator = col.iterator(); iterator.hasNext(); ) {
                        Thing thing = iterator.next();
                        list.add(thing.name);
                    }
                } else {
                    //drop
                    ag.mainCharacter.drop(selectedItem, ag.target);
                    col = adventure.getPlayer().getMe().getThings();
                    for (Iterator<Thing> iterator = col.iterator(); iterator.hasNext(); ) {
                        Thing thing = iterator.next();
                        list.add(thing.name);
                    }

                }
                ArrayAdapter adapter2 = new ArrayAdapter(c, android.R.layout.simple_list_item_1, list);
                mListView.setAdapter(adapter2);
            }
        });

        mBottomSheetBehavior1.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheet.setVisibility(View.GONE);


        Button buttonOne9 = (Button) findViewById(R.id.close10);
        final Context c2 = this;
        buttonOne9.setOnClickListener(new Button.OnClickListener() {
            public void onClick(View v) {

                grab = false;
                col = adventure.getPlayer().getMe().getThings();

                final ArrayList<String> list = new ArrayList<String>();
                for (Iterator<Thing> iterator = col.iterator(); iterator.hasNext(); ) {
                    Thing thing = iterator.next();
                    list.add(thing.name);
                }


                ArrayAdapter adapter2 = new ArrayAdapter(c2, android.R.layout.simple_list_item_1, list);
                mListView.setAdapter(adapter2);


                if (mBottomSheetBehavior1.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    bottomSheet.setVisibility(View.VISIBLE);
                    mBottomSheetBehavior1.setState(BottomSheetBehavior.STATE_EXPANDED);
                } else {
                    mBottomSheetBehavior1.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    bottomSheet.setVisibility(View.GONE);
                }
            }
        });
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Trigger the initial hide() shortly after the activity has been
        // created, to briefly hint to the user that UI controls
        // are available.
    }

    private void hide() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mControlsView.setVisibility(View.GONE);
        mVisible = false;

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        // Show the system bar
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        // Schedule a runnable to display UI elements after a delay
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Schedules a call to hide() in delay milliseconds, canceling any
     * previously scheduled calls.
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }
}

