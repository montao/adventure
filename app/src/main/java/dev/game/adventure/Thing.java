package dev.game.adventure;

/**
 * The data type for things. Subclasses will be create that takes part of the
 * story
 */
public class Thing {

	/**
	 * The name of the Thing.
	 */
	public String name = "New thing";

	/**
	 * @param name
	 *            The name of the Thing.
	 */
	Thing(String name) {
		this.name = name;
	}

}
