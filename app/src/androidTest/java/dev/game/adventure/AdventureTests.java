package dev.game.adventure;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.test.rule.ActivityTestRule;
import android.support.v7.app.AppCompatDelegate;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import static android.support.test.internal.runner.junit4.statement.UiThreadStatement.runOnUiThread;
import static junit.framework.Assert.assertNotNull;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AdventureTests {

    @Mock
    AdventureActivity aa;

    //@Mock
    //PlaceView mPlaceView;

    @Mock
    Drawable mDrawable;

    @Mock
    Resources mResources;



    @Test
    public void adventureTest0() throws Exception {
        System.out.println("test");
        try {
            runOnUiThread(new Runnable() {
                public void run() {
                    try {
                        PlaceView pv = mock(PlaceView.class);

                        when(mResources.getDrawable(R.mipmap.dungeon)).thenReturn(mDrawable);
                        when(aa.getResources()).thenReturn(mResources);
                        when(aa.getAg()).thenReturn(pv);


                        Adventure a = new Adventure(aa.scrollable, aa, aa.getAg());
                        AdventureWorld aw = new AdventureWorld(a, aa.scrollable, aa, aa.getAg());
                        Simulation myObjectUnderTest = new Simulation(aa);

                        WalkingPerson myObjectUnderTest2 = new WalkingPerson(myObjectUnderTest, aw, "Person0", 2, aa);
                        WalkingPerson myObjectUnderTest3 = new WalkingPerson(myObjectUnderTest, aw, "Person1", 2, aa);

                        myObjectUnderTest2.getThings();
                        Thing testThing = new Thing("testThing");
                        myObjectUnderTest2.getWorld().getPlace("Town").addThing(testThing);
                        myObjectUnderTest2.goTo("Town", aa);
                        myObjectUnderTest3.goTo("Town", aa);

                        myObjectUnderTest2.say("Town", aa);
                        myObjectUnderTest2.grab("testThing", aa);
                        assertThat(myObjectUnderTest2.findThing("testThing").name, is("testThing"));
                        myObjectUnderTest2.drop("testThing", aa);

                        myObjectUnderTest2.query(myObjectUnderTest3);
                        myObjectUnderTest3.attack(myObjectUnderTest2, aa);
                        myObjectUnderTest2.go("West", aa);
                        aw.randomPlace();
                        aw.connect("Town", "Dungeon", "East", "West");
                        myObjectUnderTest2.wakeup();
                        assertNotNull(aa);

                    } catch (Exception t) {
                        t.printStackTrace();
                    }
                }
            });
        } catch (Throwable t) {
            throw new Exception(t);
        }
        System.out.println("### Done test");
    }
}